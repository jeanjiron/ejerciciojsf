/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ezjamvc.modelo.dao;
//www.javamexico.org/blogs/jasuncionez
/**
 *
 * @author alumno
 */
import com.ezjamvc.modelo.dto.ArticuloDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/*** @author asuncion */
public class ArticuloDAO {
    private static final String SQL_INSERT =
            "INSERT INTO Articulo ("
            + "claveArticulo, descripcion, precio, existencias"
            + ") VALUES (?, ?, ?, ?)";
    private static final String SQL_SELECT =
            "SELECT claveArticulo, descripcion, precio, existencias "
            + "  FROM Articulo where claveArticulo= ?";
    private static final String SQL_SELECT_All =
            "SELECT claveArticulo, descripcion, precio, existencias   "
            + "FROM Articulo";
    private static final String SQL_UPDATE =
            "UPDATE Articulo SET "
            + "descripcion = ?, precio = ?, existencias = ?"
            + " WHERE "
            + "claveArticulo = ? ";
    /* SQL to delete data */
    private static final String SQL_DELETE =
            "DELETE FROM Articulo WHERE "
            + "claveArticulo = ?";
    public void create(ArticuloDTO dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(SQL_INSERT);
            ps.setString(1, dto.getClaveArticulo());
            ps.setString(2, dto.getDescripcion());
            ps.setDouble(3, dto.getPrecio());
            ps.setInt(4, dto.getExistencias());
            ps.executeUpdate();
        } finally {
            cerrar(ps);
            cerrar(conn);
        }
    }
    public ArticuloDTO load(ArticuloDTO dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(SQL_SELECT);
            ps.setString(1, dto.getClaveArticulo());
            rs = ps.executeQuery();
            List results = getResults(rs);
            if (results.size() > 0) {
                return (ArticuloDTO) results.get(0);
            } else {
                return null;
            }
        } finally {
            cerrar(rs);
            cerrar(ps);
            cerrar(conn);
        }
    }
    public List loadAll(Connection conn) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(SQL_SELECT_All);
            rs = ps.executeQuery();
            List results = getResults(rs);
            if (results.size() > 0) {
                return results;
            } else {
                return null;
            }
        } finally {
            cerrar(rs);
            cerrar(ps);
            cerrar(conn);
        }
    }
    public void update(ArticuloDTO dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(SQL_UPDATE);
            ps.setString(1, dto.getDescripcion());
            ps.setDouble(2, dto.getPrecio());
            ps.setInt(3, dto.getExistencias());
            ps.setString(4, dto.getClaveArticulo());
            ps.executeUpdate();
        } finally {
            cerrar(ps);
            cerrar(conn);
        }
    }
    public void delete(ArticuloDTO dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(SQL_DELETE);
            ps.setString(1, dto.getClaveArticulo());
            ps.executeUpdate();
        } finally {
            cerrar(ps);
            cerrar(conn);
        }
    }
    private List getResults(ResultSet rs) throws SQLException {
        List results = new ArrayList();
        while (rs.next()) {
            ArticuloDTO dto = new ArticuloDTO();
            dto.setClaveArticulo(rs.getString("claveArticulo"));
            dto.setDescripcion(rs.getString("descripcion"));
            dto.setPrecio(rs.getDouble("precio"));
            dto.setExistencias(rs.getInt("existencias"));
            results.add(dto);
        }
        return results;
    }
    private void cerrar(PreparedStatement ps) throws SQLException {
        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException e) {
            }
        }
    }
    private void cerrar(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
            }
        }
    }
    private void cerrar(Connection cnn) {
        if (cnn != null) {
            try {
                cnn.close();
            } catch (SQLException e) {
            }
        }
    }
}