/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ezjamvc.modelo.facade;

import com.ezjamvc.modelo.dao.ArticuloDAO;
import com.ezjamvc.modelo.dto.ArticuloDTO;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author alumno
 */
public class ArticuloFacade {
    private Connection cnn;
    private ArticuloDAO dao;
    public ArticuloFacade(Connection cnn) {
        this.cnn = cnn;
        dao = new ArticuloDAO();
    }
    public void crear(ArticuloDTO dto) throws SQLException {
        dao.create(dto, cnn);
    }
    public List listar() throws SQLException {
        return dao.loadAll(cnn);
    }
    public ArticuloDTO leer(ArticuloDTO dto)throws SQLException {
        return dao.load(dto, cnn);
    }
    public void actualiza(ArticuloDTO dto)throws SQLException {
         dao.update(dto, cnn);
    }
    public void elimina(ArticuloDTO dto)throws SQLException {
         dao.delete(dto, cnn);
    }
}
